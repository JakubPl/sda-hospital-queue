package pl.sda.hospital.service;

import pl.sda.hospital.model.Patient;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MyComparator implements Comparator<Patient> {
    @Override
    public int compare(Patient o1, Patient o2) {
        if(o1.getHowAngry() < o2.getHowAngry()) {
            return 1;
        } else if(o1.getHowAngry() > o2.getHowAngry()) {
            return -1;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        Patient patient1 = new Patient();
        patient1.setHowAngry(10);
        patient1.setName("Jack");
        Patient patient2 = new Patient();
        patient2.setHowAngry(5);
        patient2.setName("Joe");
        List<Patient> patients = Arrays.asList(patient2,patient1);

        System.out.println("Before sort");
        patients.forEach(e -> System.out.println(e.getName()));
        System.out.println("After sort");
        patients.sort(new MyComparator());
        patients.forEach(e -> System.out.println(e.getName()));
    }
}

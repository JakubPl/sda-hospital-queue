package pl.sda.hospital.service;

import pl.sda.hospital.model.Disease;
import pl.sda.hospital.model.Patient;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PatientGenerator {
    private static final List<String> NAMES = Arrays.asList("Jakub", "Katarzyna", "Mateusz", "Pawel", "Michal");
    private static final List<String> SURNAMES = Arrays.asList("Kowalski", "Nowak", "Podsiadlo", "Wisniewski", "Nosacz");

    public Patient get() {
        Random rand = new Random();
        final String name = PatientGenerator.NAMES.get(rand.nextInt(5));
        final String surname = PatientGenerator.SURNAMES.get(rand.nextInt(5));
        final Disease[] diseases = Disease.values();
        final Disease randomDisease = diseases[rand.nextInt(diseases.length - 1)];

        Patient patient = new Patient();
        patient.setName(name);
        patient.setSurname(surname);
        patient.setDiagnosed(randomDisease);
        patient.setHowAngry(rand.nextInt(11));
        return patient;
    }
}

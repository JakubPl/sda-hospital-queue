package pl.sda.hospital.service;

import pl.sda.hospital.model.Patient;

import java.util.PriorityQueue;

public class HospitalQueueService {
    private PriorityQueue<Patient> priorityQueue = new PriorityQueue<>(new PatientComparator());

    public void add(Patient patient) {
        priorityQueue.add(patient);
    }

    public Patient next() {
        return priorityQueue.poll();
    }

    public Patient peek() {
        return priorityQueue.peek();
    }

    public boolean isEmpty() {
        return priorityQueue.isEmpty();
    }
}

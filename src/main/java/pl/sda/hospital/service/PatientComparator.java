package pl.sda.hospital.service;

import pl.sda.hospital.model.Disease;
import pl.sda.hospital.model.Patient;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {
    public int compare(Patient patient1, Patient patient2) {
        final boolean isKowalski1 = isPatientKowalski(patient1);
        final boolean isKowalski2 = isPatientKowalski(patient2);

        if(isKowalski1 && !isKowalski2) {
            return -1;
        } else if(!isKowalski1 && isKowalski2) {
            return 1;
        }

        final boolean patient1Serious = isPatientSeriouslyIll(patient1);
        final boolean patient2Serious = isPatientSeriouslyIll(patient1);

        if(patient1Serious && !patient2Serious) {
            return -1;
        } else if(!patient1Serious && patient2Serious) {
            return 1;
        }

        final int patient1Priority = calculateSimplePriority(patient1);
        final int patient2Priority = calculateSimplePriority(patient2);

        return patient2Priority - patient1Priority;
    }

    private int calculateSimplePriority(Patient patient) {
        return patient.getDiagnosed().getInfectiousness() * patient.getHowAngry();
    }

    private boolean isPatientSeriouslyIll(Patient patient) {
        return patient.getDiagnosed().equals(Disease.SOMETHING_SEROIUS);
    }

    private boolean isPatientKowalski(Patient patient) {
        return patient.getName().equals("Kowalski");
    }
}

package pl.sda.hospital;

import pl.sda.hospital.model.Disease;
import pl.sda.hospital.model.Patient;
import pl.sda.hospital.service.HospitalQueueService;
import pl.sda.hospital.service.PatientGenerator;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class HospitalRunner {

    private static final String QUIT = "q";
    private static final String NEXT = "1";
    private static final String WHO_IS_NEXT = "2";
    private static final String NEW_PATIENT = "3";
    private static final String DEMO = "demo";
    private HospitalQueueService hospitalQueueService = new HospitalQueueService();
    private ScheduledExecutorService scheduledExecutorService;

    public void run() throws InterruptedException {
        final Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        boolean demo = false;

        while (!exit) {
            System.out.println("Menu:\n1.Nastepny\n2.Kto jest nastepny?\n3.Dodaj pacjenta\nq wyjdz\ndemo tryb demo");
            final String userInput = scanner.nextLine();
            switch (userInput) {
                case QUIT:
                    exit = true;
                    break;
                case NEXT:
                    nextPatient();
                    break;
                case WHO_IS_NEXT:
                    final Patient peek = hospitalQueueService.peek();
                    prettyPrintPatient(peek);
                    break;
                case NEW_PATIENT:
                    System.out.println("Imie");
                    final String name = scanner.nextLine();
                    System.out.println("Nazwisko");
                    final String surname = scanner.nextLine();
                    System.out.println("Jak jest z nim zle?");
                    final int howBad = Integer.valueOf(scanner.nextLine());
                    System.out.println("Zdiagnozowano na ...?");
                    final Disease diagnosed = Disease.valueOf(scanner.nextLine());

                    final Patient patient = new Patient();
                    patient.setName(name);
                    patient.setSurname(surname);
                    patient.setDiagnosed(diagnosed);
                    patient.setHowAngry(howBad);
                    addPatient(patient);
                    break;
                case DEMO:
                    System.out.println("Rozpoczynam demo!");
                    exit = true;
                    demo = true;
                    break;
                default:
                    System.out.println("No such option");
            }
        }

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        //scheduledExecutorService.schedule(new HelloWorldAfterSomeTime(), 10, TimeUnit.SECONDS);

        if (demo) {
            final PatientGenerator patientGenerator = new PatientGenerator();
            //wypelniam 10 osobami
            Stream.generate(patientGenerator::get)
                    .limit(10)
                    .forEach(this::addPatient);
            scheduledExecutorService.scheduleWithFixedDelay(() -> addPatient(patientGenerator.get()), 0, 2, TimeUnit.SECONDS);
            new GetNextPatientAndScheduleNext().run();
            /*while (!hospitalQueueService.isEmpty()) {
                Thread.sleep(2000);
                addPatient(patientGenerator.get());
                Thread.sleep(new Random().nextInt(1000));
                nextPatient();
            }*/
        }
        System.out.println("Koniec pracy");
    }

    private void addPatient(Patient patient) {
        hospitalQueueService.add(patient);
        System.out.println("Dodano:");
        prettyPrintPatient(patient);
    }

    private void nextPatient() {
        final Patient next = hospitalQueueService.next();
        System.out.println("Nastepny!:");
        prettyPrintPatient(next);
    }

    private void prettyPrintPatient(Patient peek) {
        System.out.println(peek.getName() + " " + peek.getSurname());
    }

    class GetNextPatientAndScheduleNext implements Runnable {

        @Override
        public void run() {
            nextPatient();
            scheduledExecutorService.schedule(this, getRandomDelay(), TimeUnit.MILLISECONDS);
        }

        private int getRandomDelay() {
            return new Random().nextInt(1000) + 2000;
        }
    }

}


class HelloWorldAfterSomeTime implements Runnable {

    @Override
    public void run() {
        System.out.println("Witaj swiecie po jakims czasie");
    }
}

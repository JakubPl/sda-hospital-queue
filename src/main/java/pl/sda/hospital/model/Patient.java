package pl.sda.hospital.model;

public class Patient {
    private String name;
    private String surname;
    private int howAngry;
    private Disease diagnosed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getHowAngry() {
        return howAngry;
    }

    public void setHowAngry(int howAngry) {
        this.howAngry = howAngry;
    }

    public Disease getDiagnosed() {
        return diagnosed;
    }

    public void setDiagnosed(Disease diagnosed) {
        this.diagnosed = diagnosed;
    }
}
